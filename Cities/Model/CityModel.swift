//
//  CityModel.swift
//  Cities
//
//  Created by Stanislav Volskyi on 12/9/18.
//  Copyright © 2018 Levi9. All rights reserved.
//

import Foundation
import CoreLocation

class City {
    var name: String
    var country: String?
    var location: CLLocation?
    var id: Int
    
    init(withJson json: [String: Any]) {
        name = (json["name"] as? String) ?? ""
        country = json["country"] as? String
        id = (json["id"] as? Int) ?? 0
        if let coord = json["coord"] as? [String: Double], let lon = coord["lon"], let lat = coord["lat"] {
            location = CLLocation(latitude: lat, longitude: lon)
        }
    }
}
