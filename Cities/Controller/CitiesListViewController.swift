//
//  CitiesListViewController.swift
//  Cities
//
//  Created by Stanislav Volskyi on 12/9/18.
//  Copyright © 2018 Levi9. All rights reserved.
//

import UIKit

class CitiesListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var dataSource: [City] = []
    private let dataProvider = CitiesProvider()
    
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        dataSource = dataProvider.defaultList()
    }
    
    // MARK: UI
    private func setupUI() {
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.title = "Cities"
        tableView.tableFooterView = UIView()
    }
    
    // MARK: Actions
    @IBAction func searchAction(_ sender: Any) {
        if searchBar.isFirstResponder {
            return
        }
        tableView.setContentOffset(.zero, animated: true)
        searchBar.becomeFirstResponder()
    }
    
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail", let city = sender as? City, let detailVC = segue.destination as? CityDetailViewController {
            detailVC.city = city
        }
    }
}

// MARK: UITableViewDataSource
extension CitiesListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cityCell", for: indexPath)
        let city = dataSource[indexPath.row]
        cell.textLabel?.text = city.name + (city.country == nil ? "" : ", \(city.country!)")
        return cell
    }
}

// MARK: UITableViewDelegate
extension CitiesListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let city = dataSource[indexPath.row]
        performSegue(withIdentifier: "showDetail", sender: city)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (searchBar.text ?? "").count == 0, dataSource.count - indexPath.row < 50 {
            let newData = dataProvider.nextPage()
            dataSource.append(contentsOf: newData)
            tableView.reloadData()
        }
    }
}

// MARK: UISearchBarDelegate
extension CitiesListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        dataProvider.search(for: searchText) { (result) in
            DispatchQueue.main.async {
                self.dataSource = result
                self.tableView.reloadData()
            }
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
}
