//
//  CitiesProvider.swift
//  Cities
//
//  Created by Stanislav Volskyi on 12/9/18.
//  Copyright © 2018 Levi9. All rights reserved.
//

import Foundation

class CitiesProvider {
    private let folderName = "CitiesFiles"
    var directoryUrl: URL? {
        guard let supportDir = FileManager.default.urls(for: .applicationSupportDirectory, in: .userDomainMask).first else {
            return nil
        }
        return supportDir.appendingPathComponent("CitiesFiles")
    }
    private var filesPaths: [String] = []
    private var loadedData: [[City]] = []
    private var loadedFiles: [String] = []
    
    private var searchingFile: String?
    private var searchingCities: [City] = []
    
    // MARK: Init
    init() {
        guard let folderUrl = directoryUrl else {
            return
        }
        do {
            let contents = try FileManager.default.contentsOfDirectory(atPath: folderUrl.path)
            var files: [String] = []
            for file in contents {
                files.append(folderUrl.appendingPathComponent(file).path)
            }
            filesPaths = files.sorted(by: { (file1, file2) -> Bool in
                return file1.localizedCaseInsensitiveCompare(file2) == .orderedAscending
            })
        } catch {
            AppHelper.displayError(error.localizedDescription)
        }
    }
    
    // MARK: Provider
    func defaultList() -> [City] {
        if loadedData.count > 0 {
            var cities: [City] = []
            for list in loadedData {
                cities.append(contentsOf: list)
            }
            return cities
        }
        var cities: [City] = []
        for file in filesPaths {
            let list = loadFile(file)
            loadedData.append(list)
            loadedFiles.append(file)
            cities.append(contentsOf: list)
            if cities.count > 100 {
                break
            }
        }
        return cities
    }
    
    func nextPage() -> [City] {
        if loadedFiles.count == 0 {
            return defaultList()
        }
        let lastFile = loadedFiles.last!
        let index = filesPaths.index(of: lastFile)!
        if index == filesPaths.count - 1 {
            return []
        }
        let newFile = filesPaths[index + 1]
        let list = loadFile(newFile)
        loadedData.append(list)
        loadedFiles.append(newFile)
        
        loadedData.removeFirst()
        loadedFiles.removeFirst()
        
        return list
    }
    
    func search(for searchTerm: String, completion: @escaping ([City]) -> Void) -> Void {
        if searchTerm == "" {
            searchingFile = nil
            searchingCities = []
            completion(defaultList())
            return
        }
        let startChar = searchTerm.first!
        var fileToSearch = ""
        for file in filesPaths {
            if (file as NSString).lastPathComponent.lowercased().hasPrefix("\(startChar)".lowercased()) {
                fileToSearch = file
                break
            }
        }
        if fileToSearch.count == 0 {
            completion([])
            return
        }
        
        if searchingFile != fileToSearch {
            searchingFile = fileToSearch
            DispatchQueue.global().async {
                let data = self.loadFile(fileToSearch)
                self.searchingCities = data
                completion(self.filterData(self.searchingCities, with: searchTerm))
            }
        } else {
            completion(self.filterData(searchingCities, with: searchTerm))
        }
    }
    
    func filterData(_ searchingCities: [City], with searchTerm: String) -> [City] {
        let filtered = searchingCities.filter { (city) -> Bool in
            return city.name.lowercased().hasPrefix(searchTerm.lowercased())
        }
        return filtered
    }
    
    // MARK: Load
    private func loadFile(_ path: String) -> [City] {
        do {
            let data = try Data(contentsOf: URL.init(fileURLWithPath: path))
            guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] else {
                return []
            }
            var list: [City] = []
            for value in json {
                list.append(City(withJson: value))
            }
            return list
        } catch {
            AppHelper.displayError(error.localizedDescription)
            return []
        }
    }
}
