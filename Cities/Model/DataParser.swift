//
//  DataParser.swift
//  Cities
//
//  Created by Stanislav Volskyi on 12/9/18.
//  Copyright © 2018 Levi9. All rights reserved.
//

import UIKit

class DataParser {
    private let folderName = "CitiesFiles"
    var directoryUrl: URL? {
        guard let supportDir = FileManager.default.urls(for: .applicationSupportDirectory, in: .userDomainMask).first else {
            return nil
        }
        return supportDir.appendingPathComponent("CitiesFiles")
    }
    private let tmpSecondPartFilePath = NSTemporaryDirectory() + "secondPart_tmp.json"
    
    private let dataParsedKey = "dataParsedKey"
    private var parsingQueue = DispatchQueue.global(qos: .userInteractive)
    
    var shouldPrepareData: Bool {
        return !UserDefaults.standard.bool(forKey: dataParsedKey)
    }
    
    func prepareData(_ completion: @escaping ((Error?) -> Void)) {
        // As we have a huge and unsorted json file I decided to split it into sorted smaller files
        // Each file will contain only cities starting with the same letter
        // Then we can load this small file into memory and have a realtime search
        
        // This operation will take 1-2 minutes so it is performed in async queue
        // As loading app time doesn't matters I'm using serial queue. Splitting this operation into few concurrent threads
        // can give better performance
        
        // This operation should be done only once. Next time app will load immediately
        parsingQueue.async {
            guard let path = Bundle.main.path(forResource: "cities", ofType: "json") else {
                completion(self.error(withCode: 404, description: "Data file was not found"))
                return
            }
            do {
                // Load full json into memory. If json were 2x lagrer it could produce memory issue
                // and extremely large files should be read by chunks. (e.g.) use input stream, read a chunk, parse it...
                let data = try Data(contentsOf: URL.init(fileURLWithPath: path))
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] else {
                    throw(self.error(withCode: 500, description: "Invalid json structure"))
                }
                // split file on 2 parts
                let middle = json.count / 2
                let firstPart = json[0..<middle]
                let secondPart = json[middle..<json.count]
                
                // Check if total entries count is correct
                if firstPart.count + secondPart.count != json.count {
                    throw(self.error(withCode: 500, description: "Parser failed. Invalid cities count after processing"))
                }
                
                // Save second part for parsing later. It will free 1/2 memory that json takes
                let secondPartData = try JSONSerialization.data(withJSONObject: Array(secondPart), options: [])
                let fileManager = FileManager.default
                if fileManager.fileExists(atPath: self.tmpSecondPartFilePath) {
                    try fileManager.removeItem(atPath: self.tmpSecondPartFilePath)
                }
                fileManager.createFile(atPath: self.tmpSecondPartFilePath, contents: secondPartData, attributes: [:])
                
                // create new task in queue to free all local variables from current task
                self.parsingQueue.async {
                    self.sort(json: Array(firstPart), completion: { (error: Error?) in
                        if let error = error {
                            completion(error)
                        } else {
                            self.parsingQueue.async {
                                self.parseSecondPart(completion)
                            }
                        }
                    })
                }
            } catch {
                completion(error)
            }
        }
    }
    
    private func parseSecondPart(_ completion: @escaping ((Error?) -> Void)) {
        do {
            let data = try Data(contentsOf: URL.init(fileURLWithPath: tmpSecondPartFilePath))
            guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] else {
                throw(self.error(withCode: 500, description: "Invalid json structure"))
            }
            try FileManager.default.removeItem(atPath: self.tmpSecondPartFilePath)
            self.sort(json: json, completion: { (error: Error?) in
                if error == nil {
                    UserDefaults.standard.set(true, forKey: self.dataParsedKey)
                    UserDefaults.standard.synchronize()
                }
                completion(error)
            })
        } catch {
            completion(error)
        }
    }
    
    private func sort(json: [[String: Any]], completion: @escaping ((Error?) -> Void)) {
        // Sorting values in alphabetical order
        // Using set to keep all First letters. Set guarantee that each entry will be stored only once in it
        // It will be files names and used for search
        var firstLetters: Set<Character> = []
        let sorted = json.sorted { (item1, item2) -> Bool in
            if let name1 = item1["name"] as? String, let name2 = item2["name"] as? String {
                firstLetters.insert(name1.lowercased().first!)
                firstLetters.insert(name2.lowercased().first!)
                return name1.localizedCaseInsensitiveCompare(name2) == .orderedAscending
            }
            return false
        }
        
        guard let folderUrl = directoryUrl else {
            completion(error(withCode: 500, description: "Unable to create application support directory"))
            return
        }
        do {
            let fileManager = FileManager.default
            if !fileManager.fileExists(atPath: folderUrl.path, isDirectory: nil) {
                try fileManager.createDirectory(atPath: folderUrl.path, withIntermediateDirectories: true, attributes: nil)
            }
            // now for every first letter get all entries which name starts with it and store into file
            for char in firstLetters {
                // to release data inside loop and prevent memory warnings
                autoreleasepool {
                    do {
                        var fileUrl = folderUrl
                        fileUrl.appendPathComponent("\(char).json")
                        var filteredCities = sorted.filter { (item) -> Bool in
                            if let name = item["name"] as? String, let firstChar = name.lowercased().first, firstChar == char {
                                return true
                            }
                            return false
                        }
                        if fileManager.fileExists(atPath: fileUrl.path), let fileData = fileManager.contents(atPath: fileUrl.path) {
                            // For second part. Load entries, add new ones. Sort them and save
                            if let existingJson = try JSONSerialization.jsonObject(with: fileData, options: []) as? [[String: Any]] {
                                filteredCities.append(contentsOf: existingJson)
                                filteredCities = filteredCities.sorted(by: { (item1, item2) -> Bool in
                                    if let name1 = item1["name"] as? String, let name2 = item2["name"] as? String {
                                        firstLetters.insert(name1.lowercased().first!)
                                        firstLetters.insert(name2.lowercased().first!)
                                        return name1.localizedCaseInsensitiveCompare(name2) == .orderedAscending
                                    }
                                    return false
                                })
                                try fileManager.removeItem(atPath: fileUrl.path)
                                print("Updating file \(fileUrl.path)")
                            } else {
                                completion(error(withCode: 1001, description: "Parsing failed. Can not read existing data"))
                                return
                            }
                        } else {
                            print("Saving file \(fileUrl.path)")
                        }
                        
                        let data = try JSONSerialization.data(withJSONObject: filteredCities, options: [])
                        fileManager.createFile(atPath: fileUrl.path, contents: data, attributes: [:])
                    } catch {
                        completion(error)
                    }
                }
            }
            completion(nil)
        } catch {
            completion(error)
        }
    }
    
    func cleanData(_ completion: @escaping ((Error?) -> Void)) {
        // if error occured we have to remove all files
        let fileManager = FileManager.default
        guard let folderUrl = directoryUrl else {
            completion(error(withCode: 500, description: "Unable to create documents directory"))
            return
        }
        if !fileManager.fileExists(atPath: folderUrl.path, isDirectory: nil) {
            return
        }
        do {
            try fileManager.removeItem(atPath: self.tmpSecondPartFilePath)
            let contents = try fileManager.contentsOfDirectory(atPath: folderUrl.path)
            for file in contents {
                let url = folderUrl.appendingPathComponent(file)
                try fileManager.removeItem(atPath: url.path)
            }
            completion(nil)
        } catch {
            completion(error)
        }
    }
    
    // MARK: Errors
    private func error(withCode code: Int, description: String) -> Error {
        return NSError(domain: "com.levi9.citiesApp", code: code, userInfo: [NSLocalizedDescriptionKey: description])
    }
}
