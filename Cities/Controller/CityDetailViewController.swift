//
//  CityDetailViewController.swift
//  Cities
//
//  Created by Stanislav Volskyi on 12/10/18.
//  Copyright © 2018 Levi9. All rights reserved.
//

import UIKit
import MapKit

class CityDetailViewController: UIViewController {
    @IBOutlet weak var mapView: MKMapView!
    var city: City!
    
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if let location = city.location {
            let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
            let region = MKCoordinateRegion(center: location.coordinate, span: span)
            mapView.setRegion(region, animated: true)
        } else {
            AppHelper.displayError("Location not found")
        }
        navigationItem.title = city.name + (city.country == nil ? "" : ", \(city.country!)")
    }

}
