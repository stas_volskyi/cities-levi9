//
//  AppHelper.swift
//  Cities
//
//  Created by Stanislav Volskyi on 12/9/18.
//  Copyright © 2018 Levi9. All rights reserved.
//

import UIKit

class AppHelper {
    // MARK: Error handling
    class func displayError(_ message: String) {
        displayAlert(title: "Error", message: message)
    }
    
    class func displayAlert(title: String?, message: String?, cancelButtonName: String = "Ok", cancelAction: ((UIAlertAction) -> Void)? = nil, okButtonName: String? = nil, okAction: ((UIAlertAction) -> Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: cancelButtonName, style: .cancel, handler: cancelAction)
        alertController.addAction(cancelAction)
        if let okButtonName = okButtonName, let okAction = okAction {
            let okAlertAction = UIAlertAction(title: okButtonName, style: .default, handler: okAction)
            alertController.addAction(okAlertAction)
        }
        let rootVC = UIApplication.shared.keyWindow?.rootViewController
        let topVC = rootVC?.presentedViewController ?? rootVC
        topVC?.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: View controller
    class func present(viewController: UIViewController, animated: Bool = true) {
        topViewController()?.present(viewController, animated: animated, completion: nil)
    }
    
    class func topViewController() -> UIViewController? {
        var topVC = UIApplication.shared.keyWindow?.rootViewController
        while topVC?.presentedViewController != nil {
            topVC = topVC?.presentedViewController
        }
        return topVC
    }
}
