//
//  ViewController.swift
//  Cities
//
//  Created by Stanislav Volskyi on 12/9/18.
//  Copyright © 2018 Levi9. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController {
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var dotsLabel: UILabel!
    @IBOutlet weak var container: UIView!
    
    private let parser = DataParser()
    private var textTimer: Timer?
    
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareData()
    }
    
    // MARK: UI
    private func showTextAnimation(_ show: Bool) {
        if !show {
            textTimer?.invalidate()
            return
        }
        var dotsCounter = 1
        textTimer = Timer.scheduledTimer(withTimeInterval: 0.7, repeats: true, block: { (timer) in
            var text = ""
            for _ in 0...dotsCounter - 1 {
                text += "."
            }
            self.dotsLabel.text = text
            dotsCounter += 1
            if dotsCounter > 3 {
                dotsCounter = 1
            }
        })
    }
    
    // MARK: Navigation
    private func showCitiesList() {
        if let listVC = storyboard?.instantiateViewController(withIdentifier: "citiesViewController") {
            navigationController?.setViewControllers([listVC], animated: true)
        }
    }

    // MARK: Data
    func prepareData() {
        if !parser.shouldPrepareData {
            container.alpha = 0
            showCitiesList()
            return
        }
        showTextAnimation(true)
        parser.prepareData { (error) in
            DispatchQueue.main.async {
                self.showTextAnimation(false)
                if let error = error {
                    AppHelper.displayError(error.localizedDescription)
                    self.parser.cleanData({ (error) in
                        if let error = error {
                            AppHelper.displayError(error.localizedDescription)
                        }
                    })
                } else {
                    self.showCitiesList()
                }
            }
        }
    }
}

