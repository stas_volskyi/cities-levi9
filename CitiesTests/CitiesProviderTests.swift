//
//  CitiesProviderTests.swift
//  CitiesTests
//
//  Created by Stanislav Volskyi on 12/10/18.
//  Copyright © 2018 Levi9. All rights reserved.
//

import XCTest
@testable import Cities

class CitiesProviderTests: XCTestCase {
    var testList: [City] = []
    
    override func setUp() {
        testList.removeAll()
        testList.append(City(withJson: ["name": "Alabama", "country": "US"]))
        testList.append(City(withJson: ["name": "Albuquerque", "country": "US"]))
        testList.append(City(withJson: ["name": "Anaheim", "country": "US"]))
        testList.append(City(withJson: ["name": "Arizona", "country": "US"]))
        testList.append(City(withJson: ["name": "Sydney", "country": "AU"]))
    }

    func testCorrectSearch() {
        let testProvider = CitiesProvider()
        var result = testProvider.filterData(testList, with: "Uru")
        XCTAssert(result.count == 0)
        
        result = testProvider.filterData(testList, with: "al")
        XCTAssert(result.count == 2)
        
        result = testProvider.filterData(testList, with: "arIZO")
        XCTAssert(result.count == 1)
        
        result = testProvider.filterData(testList, with: "s")
        XCTAssert(result.count == 1)
    }
}
